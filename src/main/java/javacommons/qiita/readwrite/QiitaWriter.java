package javacommons.qiita.readwrite;

import com.gitlab.javacommons.paizadb.PaizaDb;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import db.qiita.Batch;
import db.qiita.Item;
import javacommons.qiita.QiitaAccess;
import javacommons.qiita.QiitaUtil;

import java.util.List;
import java.util.Optional;

public class QiitaWriter extends QiitaAccess {
    private PaizaDb.DbType dbType;

    public QiitaWriter(PaizaDb.DbType dbType) {
        super(dbType);
    }

    public void saveToBatch() throws Exception {
        try (JdbcConnectionSource connectionSource = this.getJdbcConnectionSource()) {
            Dao<Item, String> itemDao = DaoManager.createDao(connectionSource, Item.class);
            Dao<Batch, String> batchDao = DaoManager.createDao(connectionSource, Batch.class);
            QueryBuilder<Item, String> qb = itemDao.queryBuilder();
            qb.orderBy("batch", false);
            Optional<Item> first = itemDao.query(qb.prepare()).stream().findFirst();
            if (first.isPresent()) {
                qb = itemDao.queryBuilder();
                qb.where().ne("batch", first.get().batch);
                List<Item> notUpToDate = itemDao.query(qb.prepare());
                if (batchDao.queryForId(first.get().batch) == null) {
                    List<Item> items = itemDao.queryForAll();
                    String json = QiitaUtil.toJson(items);
                    Batch batch = new Batch(first.get().batch);
                    batch.count = notUpToDate.size();
                    batch.json = json;
                    batch.ts = QiitaUtil.formatCurrentTimestamp();
                    batchDao.create(batch);
                }
            }
        }
    }
}
