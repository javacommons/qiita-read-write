package javacommons.qiita.readwrite;

import com.gitlab.javacommons.paizadb.PaizaDb;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import db.qiita.Item;
import javacommons.qiita.QiitaAccess;

import java.util.List;

public class QiitaReader extends QiitaAccess {
    public QiitaReader(PaizaDb.DbType dbType) {
        super(dbType);
    }

    public List<Item> readLatestItems(long offset, long limit) throws Exception {
        try (JdbcConnectionSource connectionSource = this.getJdbcConnectionSource()) {
            Dao<Item, String> dao = DaoManager.createDao(connectionSource, Item.class);
            QueryBuilder<Item, String> qb = dao.queryBuilder();
            qb.where().isNotNull("created");
            qb.orderBy("created", false);
            qb.offset(offset).limit(limit);
            return dao.query(qb.prepare());
        }
    }
}
